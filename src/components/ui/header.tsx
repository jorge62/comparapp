"use client";
import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import CurrencyExchangeIcon from "@mui/icons-material/CurrencyExchange";
import { useRouter } from "next/navigation";
import Modal from "@mui/material/Modal";
import Link from "next/link";

const pages = ["Calcular", "Comparar"];

const Header: React.FC = () => {
  const router = useRouter();

  const [open, setOpen] = React.useState(false);

  const [openContact, setOpenContact] = React.useState(false);

  const handleOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);

  const handleCloseContact = () => setOpenContact(false);

  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
    null
  );
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = (page: string) => {
    if (typeof page === "string") {
      router.push(`/${page.toLowerCase()}`);
    }
    setAnchorElNav(null);
  };

  const handleOpenContact = () => {
    setOpenContact(true);
  };

  return (
    <>
      <AppBar position="absolute" sx={{ width: "100vw" }}>
        <Container maxWidth={false}>
          <Toolbar disableGutters>
            <Box display={"flex"}>
              <Box
                sx={{
                  display: { xs: "none", sm: "flex" },
                }}
              >
                <Box
                  display={"flex"}
                  flexDirection={"row"}
                  // sx={{
                  //   marginLeft: { sm: "-100px", md: "-170px" },
                  // }}
                  textAlign={"left"}
                >
                  <Link
                    href={"/"}
                    style={{
                      textDecoration: "none",
                      color: "#fff",
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    <CurrencyExchangeIcon
                      sx={{ display: { xs: "none", md: "flex" }, mr: 1 }}
                    />

                    <Typography
                      noWrap
                      sx={{
                        mr: 2,
                        display: { xs: "none", md: "flex" },
                        fontFamily: "monospace",
                        fontWeight: 700,
                        letterSpacing: ".3rem",
                        color: "inherit",
                        textDecoration: "none",
                        fontSize: { xs: "14px", md: "24px" },
                      }}
                    >
                      ComparApp
                    </Typography>
                  </Link>
                </Box>
              </Box>
              <Box
                sx={{
                  flexGrow: 1,
                  display: { xs: "flex", md: "none" },
                }}
              >
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleOpenNavMenu}
                  color="inherit"
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorElNav}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  open={Boolean(anchorElNav)}
                  onClose={handleCloseNavMenu}
                  sx={{
                    display: { xs: "block", md: "none" },
                  }}
                >
                  {pages.map((page) => (
                    <MenuItem
                      key={page}
                      onClick={() => handleCloseNavMenu(page)}
                    >
                      <Typography
                        textAlign="center"
                        textTransform={"capitalize"}
                      >
                        {page}
                      </Typography>
                    </MenuItem>
                  ))}
                  <Button
                    onClick={handleOpen}
                    sx={{ ml: 1, color: "black", display: "block" }}
                  >
                    <Typography textTransform={"capitalize"}>
                      Acerca de
                    </Typography>
                  </Button>
                  <Button
                    onClick={handleOpenContact}
                    sx={{ ml: 1, color: "black", display: "block" }}
                  >
                    <Typography textTransform={"capitalize"}>
                      Contacto
                    </Typography>
                  </Button>
                </Menu>
              </Box>
            </Box>
            <Box>
              <Box
                display={"flex"}
                flexDirection={"row"}
                sx={{
                  marginLeft: { xs: "140px", sm: "200px" },
                }}
              >
                <CurrencyExchangeIcon
                  sx={{ display: { xs: "flex", md: "none" }, mr: 1 }}
                />
                <Link
                  href={"/"}
                  style={{ textDecoration: "none", color: "#fff" }}
                >
                  <Typography
                    noWrap
                    sx={{
                      mr: 2,
                      display: { xs: "flex", md: "none" },
                      flexGrow: 1,
                      fontFamily: "monospace",
                      fontWeight: 700,
                      letterSpacing: ".3rem",
                      color: "inherit",
                      textDecoration: "none",
                      fontSize: { xs: "14px", md: "24px" },
                    }}
                  >
                    ComparApp
                  </Typography>
                </Link>
              </Box>
              <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
                {pages.map((page: string) => (
                  <Button
                    key={page}
                    onClick={() => handleCloseNavMenu(page)}
                    sx={{ my: 2, color: "white", display: "block" }}
                  >
                    <Typography textTransform={"capitalize"}>{page}</Typography>
                  </Button>
                ))}
                <Button
                  onClick={handleOpen}
                  sx={{ my: 2, color: "white", display: "block" }}
                >
                  <Typography textTransform={"capitalize"}>
                    Acerca de
                  </Typography>
                </Button>
                <Button
                  onClick={handleOpenContact}
                  sx={{ my: 2, color: "white", display: "block" }}
                >
                  <Typography textTransform={"capitalize"}>Contacto</Typography>
                </Button>
              </Box>
            </Box>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box
                sx={{
                  position: "absolute" as "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)",
                  width: 400,
                  bgcolor: "background.paper",
                  border: "2px solid #000",
                  boxShadow: 24,
                  p: 4,
                }}
              >
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  ComparaApp V.1.0
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Calcula cuento estas ganando en relacion al dolar y encuentra
                  los sueldos que se estan pagando en el mundo IT.
                </Typography>
              </Box>
            </Modal>
            <Modal
              open={openContact}
              onClose={handleCloseContact}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box
                sx={{
                  position: "absolute" as "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)",
                  width: 400,
                  bgcolor: "background.paper",
                  border: "2px solid #000",
                  boxShadow: 24,
                  p: 4,
                }}
              >
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  Datos de contacto
                </Typography>
                <Typography
                  id="modal-modal-description"
                  sx={{ mt: 2, fontWeight: "bold" }}
                >
                  Mail: jorge1920merida@gmail.com
                  <br></br>
                  GitLab User: jorge62
                </Typography>
              </Box>
            </Modal>
          </Toolbar>
        </Container>
      </AppBar>
    </>
  );
};
export default Header;
