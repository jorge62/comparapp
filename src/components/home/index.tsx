"use client";
import React from "react";
import { motion as m } from "framer-motion";
import { container } from "../../utils/animation";
import Link from "next/link";
import { AnimatedTextWord } from "../ui/animatedWord";
import { AnimatedCharacter } from "../ui/animatedCharacter";
import styles from "./styles.module.scss";
import { Box, Typography } from "@mui/material";
import Image from "next/image";
import useMediaQuery from "@mui/material/useMediaQuery";

export const Home: React.FC = () => {
  const matches = useMediaQuery("(min-width:600px)");

  return (
    <Box className={styles.section}>
      <div className={styles.container}>
        <m.div
          className={styles.motionDiv}
          variants={container}
          initial="hidden"
          animate="show"
          style={{ fontSize: matches ? "80px" : "40px" }}
        >
          <AnimatedCharacter text="ComparApp" />
        </m.div>
        <br></br>
        <m.div style={{ fontSize: matches ? "80px" : "20px" }}>
          <Box sx={{ color: "#fff" }}>
            <AnimatedTextWord text="Calcula tu sueldo en relación al dolar ó comparalo con otros" />
          </Box>
        </m.div>
        <Box display={"flex"} flexDirection={"row"} gap={20}>
          <m.div
            whileHover={{ scale: 1.4 }}
            whileTap={{ scale: 1 }}
            style={{
              marginTop: "106px",
            }}
          >
            <Link href="/calcular" className={styles.link}>
              <Typography
                color={"#FFF"}
                sx={{ fontSize: { xs: "18px", sm: "34px" } }}
              >
                Calcular
              </Typography>
            </Link>
          </m.div>
          <m.div
            whileHover={{ scale: 1.4 }}
            whileTap={{ scale: 1 }}
            style={{
              marginTop: "106px",
            }}
          >
            <Link href="/comparar" className={styles.link}>
              <Typography
                color={"#FFF"}
                sx={{ fontSize: { xs: "18px", sm: "34px" } }}
              >
                Comparar
              </Typography>
            </Link>
          </m.div>
        </Box>
      </div>
    </Box>
  );
};
