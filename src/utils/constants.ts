export const steps = [
  {
    id: 1,
    description: `Ingresa el suelde neto y la fecha aproximada desde la cual lo cobrabas`,
    name: "firstSalary",
  },
  {
    description: "Ingresa tú sueldo neto actual",
    id: 2,
    name: "secondSalary",
  },
];

export const seniority = [
  {
    name: "Junior",
  },
  { name: "Ssr" },
  { name: "Sr" },
  { name: "Todos" },
];

export const perfilData = [
  {
    name: "Frontend Developer",
  },
  { name: "Backend Developer" },
  { name: "Ing DevOps" },
  { name: "Data Science y Business Intelligence" },
  { name: "Diseñador de Experiencia de Usuario (UX)" },
  { name: "Especialista en Redes" },
  { name: "Profesional de la Ciberseguridad" },
  { name: "Arquitecto de Soluciones" },
  { name: "Full-Stack" },
  { name: "Todos" },
];
