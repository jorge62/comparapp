import create from "zustand";

//entidad para la store de datos

interface ILastDataStore {
  valueSellDollar: number;
  valueSellPesos: number;
  updateValueSellDollar: (arg: any) => void;
  updateValueSellPesos: (arg: any) => void;
}

export const useDataStoreDollar = create<ILastDataStore>((set, get) => ({
  valueSellDollar: 0,
  valueSellPesos: 0,
  updateValueSellDollar: async (value) => {
    set({ valueSellDollar: value });
  },
  updateValueSellPesos: async (value) => {
    set({ valueSellPesos: value });
  },
}));
