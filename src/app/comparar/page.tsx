"use client";

import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Header from "@/components/ui/header";
import {
  Box,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
  useMediaQuery,
} from "@mui/material";
import RootLayout from "../layout";
import personsData from "@/utils/dataComparApp.json";
import { DataPerson } from "@/models";
import Link from "next/link";
import { seniority } from "@/utils/constants";
import { perfilData } from "@/utils/constants";

const Compare: React.FC = () => {
  const [perfil, setPerfil] = React.useState("");

  const [isClient, setIsClient] = React.useState(false);

  const matches = useMediaQuery("(min-width:600px)");

  const [perfilString, setPerfilString] = React.useState("");

  const [seniorityData, setSeniority] = React.useState("");

  const [seniorityString, setSeniorityString] = React.useState("");

  const [persons, setPersons] = React.useState<DataPerson[]>(personsData);

  React.useEffect(() => {
    setIsClient(true);
  }, []);

  const handleChange = (event: SelectChangeEvent) => {
    setPerfil(event.target.value as string);
  };

  const handleItem = (p: string) => {
    if (p === "Todos") {
      setPerfilString(p);
      let filterList = [...personsData];
      if (seniorityString === "") {
        filterList = personsData.filter((item) => item.perfil === p);
      } else if (seniorityString == "Todos") {
        filterList = personsData;
      } else {
        filterList = personsData.filter(
          (item) => item.senioriy === seniorityString
        );
      }
      setPersons(filterList);
    } else {
      setPerfilString(p);
      let filterList = [...personsData];
      if (p !== perfilString) {
        if (seniorityString === "" || seniorityString === "Todos") {
          filterList = personsData.filter((item) => item.perfil === p);
        } else {
          filterList = personsData.filter(
            (item) => item.perfil === p && item.senioriy === seniorityString
          );
        }
      } else {
        filterList = personsData.filter(
          (item) => item.perfil === p && item.senioriy === seniorityString
        );
      }
      setPersons(filterList);
    }
  };

  const handleChangeSeniority = (event: SelectChangeEvent) => {
    setSeniority(event.target.value as string);
  };

  const handleItemSeniority = (p: string) => {
    if (p === "Todos") {
      setSeniorityString(p);
      let filterList = [...personsData];
      if (perfilString.length > 0 && perfilString !== "Todos") {
        filterList = personsData.filter((item) => item.perfil === perfilString);

        setPersons(filterList);
      } else {
        setPersons(personsData);
      }
    } else {
      setSeniorityString(p);
      let filterList = [...personsData];
      if (p !== seniorityString) {
        if (perfilString === "" || perfilString === "Todos") {
          filterList = personsData.filter((item) => item.senioriy === p);
        } else {
          filterList = personsData.filter(
            (item) => item.senioriy === p && item.perfil === perfilString
          );
        }
        setPersons(filterList);
      }
    }
  };

  return (
    <>
      {isClient && (
        <Box
          sx={{ backgroundColor: "#004F92" }}
          style={{
            height: "100%",
            width: "100%",
            overflow: "hidden",
            justifyContent: "center",
          }}
        >
          <Header />
          <Box
            display={"flex"}
            flexDirection={"column"}
            justifyContent={"center"}
            textAlign={"center"}
            alignContent={"center"}
            alignItems={"center"}
          >
            <Link
              href={"https://forms.gle/JYExAxwD29Zn7eZs6"}
              style={{ textDecorationColor: "white" }}
              rel="noopener noreferrer"
              target="_blank"
            >
              <Box
                display={"flex"}
                justifyContent={"center"}
                textAlign={"center"}
                marginTop={"100px"}
                color={"#1AA7EC"}
                fontWeight={"bold"}
                lineHeight={"1.5"}
                sx={{
                  minWidth: { xs: "200px", sm: "400px" },
                }}
              >
                <Typography
                  component={"span"}
                  sx={{
                    fontSize: { xs: "16px", sm: "24px" },
                  }}
                >
                  Podés compartir tus datos através de este formulario
                  <Typography
                    sx={{
                      fontSize: { xs: "16px", sm: "24px" },
                      padding: "10px",
                    }}
                    component={"span"}
                    color={"#ffff"}
                  >
                    https://forms.gle/8HoYXsfwyaJi9fVX9
                  </Typography>
                  y ampliar la informacion para toda la comunidad
                </Typography>
              </Box>
            </Link>
            <TableContainer
              component={Paper}
              sx={{
                marginTop: "100px",
                backgroundColor: "#1976d2",
              }}
            >
              <Table
                sx={{ minWidth: 900, margin: "10px" }}
                aria-label="simple table"
              >
                <TableHead>
                  <TableRow>
                    <TableCell
                      align="left"
                      sx={{
                        width: { sm: "100px", md: "400px", color: "#fff" },
                      }}
                    >
                      <Typography
                        component={"span"}
                        id="demo-simple-select-label"
                        sx={{ display: "inline-block", marginRight: "10px" }}
                      >
                        Seniority
                      </Typography>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={seniorityData}
                        label="Seniority"
                        onChange={handleChangeSeniority}
                        style={{ maxHeight: "30px", minWidth: "50px" }}
                      >
                        {seniority.map(
                          (row: { name: string }, index: number) => (
                            <MenuItem
                              key={index}
                              value={10 * (index + 1)}
                              onClick={(e) => handleItemSeniority(row.name)}
                            >
                              {row.name}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        width: { sm: "100px", md: "400px", color: "#fff" },
                      }}
                    >
                      <Typography
                        component={"span"}
                        id="demo-simple-select-label"
                        sx={{ display: "inline-block", marginRight: "10px" }}
                      >
                        Perfil
                      </Typography>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={perfil}
                        label="Perfil"
                        onChange={handleChange}
                        style={{ maxHeight: "30px", minWidth: "50px" }}
                      >
                        {perfilData.map(
                          (row: { name: string }, index: number) => (
                            <MenuItem
                              key={index}
                              value={10 * (index + 1)}
                              onClick={(e) => handleItem(row.name)}
                            >
                              {row.name}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        width: { sm: "100px", md: "400px", color: "#fff" },
                      }}
                    >
                      Lenguaje
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        width: { sm: "100px", md: "400px", color: "#fff" },
                      }}
                    >
                      Años de experiencia
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        width: { sm: "100px", md: "400px", color: "#fff" },
                      }}
                    >
                      Sueldo
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        width: { sm: "100px", md: "400px", color: "#fff" },
                      }}
                    >
                      Empresa
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {persons.map((row: DataPerson, index: number) => (
                    <TableRow
                      key={index}
                      sx={{
                        "&:last-child td, &:last-child th": { border: 0 },
                        backgroundColor: "#fff",
                      }}
                    >
                      <TableCell align="left">{row.senioriy!}</TableCell>
                      <TableCell align="left">{row.perfil!}</TableCell>
                      <TableCell align="left">{row.lenguaje!}</TableCell>
                      <TableCell align="left">{row.anios!}</TableCell>
                      <TableCell align="left">
                        {row
                          .sueldo!?.toString()
                          .replace(/[^0-9]/g, "")
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                      </TableCell>
                      <TableCell align="left">{row.empresa!}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      )}
    </>
  );
};

export default Compare;
