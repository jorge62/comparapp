"use client";

import * as React from "react";
import styles from "./styles.module.scss";
import {
  Box,
  Button,
  CircularProgress,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import { useRouter } from "next/navigation";
import { useDataStoreDollar } from "@/store/useDataStore";
import RootLayout from "../layout";
import Header from "@/components/ui/header";

const Calculate: React.FC = () => {
  const { updateValueSellDollar, valueSellDollar, updateValueSellPesos } =
    useDataStoreDollar();

  const [calculateInput, setCalculateInput] = React.useState<string>("");

  const [calculateButton, setCalculateButton] = React.useState<boolean>(false);

  const [errorInput, setErrorInput] = React.useState<boolean>(false);

  const [showSpinner, setShowSpinner] = React.useState<boolean>(false);

  const router = useRouter();

  React.useEffect(() => {
    if (parseFloat(calculateInput) > 0) {
      setCalculateButton(false);
    } else {
      setCalculateButton(true);
    }
  }, [calculateInput]);

  const handleCalculate = async () => {
    setShowSpinner(true);
    try {
      const res = await fetch("https://api.bluelytics.com.ar/v2/latest", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "GET",
      });
      const data = await res.json();
      if (data) {
        updateValueSellDollar(
          Number(calculateInput.split(".").join("")) / data.blue.value_sell
        );
        updateValueSellPesos(Number(calculateInput.split(".").join("")));
        router.push("/resultado");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const onChangeCalculate = (e: any) => {
    const value = e.target.value
      ?.toString()
      .replace(/[^0-9]/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".");

    setCalculateInput(value);
  };

  const handleKeyDown = (event: any) => {
    if (event.key === "Enter") {
      handleCalculate();
    }
  };

  return (
    <>
      <Box
        sx={{ backgroundColor: "#004F92" }}
        style={{
          height: "100%",
          width: "100%",
          overflow: "hidden",
          justifyContent: "center",
        }}
      >
        <Header />
        <Box
          className={styles.container}
          sx={{ marginTop: { xs: "151px", sm: "220px" } }}
        >
          {!showSpinner ? (
            <Box
              className={styles.cardContainer}
              sx={{
                width: { xs: "300px", md: "700px" },

                height: { xs: "300px", md: "400px" },
              }}
            >
              <Typography
                component={"span"}
                sx={{ color: "#fff", fontSize: { xs: "25px", md: "40px" } }}
              >
                Ingresa tu suelo Neto
              </Typography>
              <TextField
                id="outlined-basic"
                sx={{
                  borderColor: "#fff",
                  color: "#fff",
                  backgroundColor: "#fff",
                  width: { xs: "240px", sm: "320px" },
                }}
                label=""
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <AttachMoneyIcon />
                    </InputAdornment>
                  ),
                }}
                variant="outlined"
                value={calculateInput
                  ?.toString()
                  .replace(/[^0-9]/g, "")
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                onChange={(e) => onChangeCalculate(e)}
                onKeyDown={handleKeyDown}
              />
              <Button
                sx={{
                  width: { xs: "200px", sm: "260px" },
                  marginTop: { xs: "10px", sm: "30px" },
                  backgroundColor: "#fff",
                }}
                variant="outlined"
                disabled={calculateButton}
                onClick={handleCalculate}
              >
                <Typography
                  component={"span"}
                  color={calculateButton ? "#BEBEBE" : "#4361EE"}
                  fontStyle={"normal"}
                  fontSize={17}
                  fontWeight={"bold"}
                >
                  Calcular
                </Typography>
              </Button>
            </Box>
          ) : (
            <Box sx={{ marginTop: { xs: "100px", md: "150px" } }}>
              <CircularProgress size={70} />
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
};

export default Calculate;
