"use client";

import { useFollowPointer } from "@/hooks/use-Follow-Pointer";
import { useDataStoreDollar } from "@/store/useDataStore";
import { Box, Button, Typography, useMediaQuery } from "@mui/material";
import styles from "./styles.module.scss";
import { motion as m } from "framer-motion";
import Link from "next/link";
import * as React from "react";
import Image from "next/image";
import RootLayout from "../layout";
import Header from "@/components/ui/header";

interface Props {}

const Result: React.FC<Props> = () => {
  const { valueSellDollar, valueSellPesos } = useDataStoreDollar();
  const matches = useMediaQuery("(min-width:600px)");
  const ref = React.useRef(null);
  const { x, y } = useFollowPointer(ref);

  return (
    <>
      <Box
        sx={{ backgroundColor: "#004F92" }}
        style={{
          height: "100%",
          width: "100%",
          overflow: "hidden",
          justifyContent: "center",
        }}
      >
        <Header />
        <Box>
          <Box
            className={styles.container}
            sx={{
              flexDirection: { xs: "column", md: "row" },
              marginTop: { xs: "0px", md: "200px" },
              gap: { xs: "10px", md: "50px" },
            }}
          >
            <Box
              className={styles.cardContainer}
              sx={{
                width: { xs: "300px", md: "700px" },
                marginTop: { xs: "100px", md: "0px" },
                height: { xs: "250px", md: "400px" },
              }}
            >
              <Typography fontSize={matches ? 34 : 24} sx={{ color: "#FFF" }}>
                Sueldo en pesos :{!matches && <br></br>} ${" "}
                {valueSellPesos
                  ?.toString()
                  .replace(/[^0-9]/g, "")
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
              </Typography>
              <Box
                display={"flex"}
                justifyContent={"center"}
                flexDirection={"row"}
                gap={10}
                sx={{ marginTop: { xs: "20px", md: "50px" } }}
              >
                <Link href={"/comparar"} style={{ textDecoration: "none" }}>
                  <Button variant="outlined">
                    <Typography
                      fontStyle={"normal"}
                      fontSize={matches ? 19 : 12}
                      fontWeight={"bold"}
                      color={"#FFF"}
                    >
                      Comparar mi sueldo
                    </Typography>
                  </Button>
                </Link>
              </Box>
            </Box>

            <Box
              className={styles.cardContainer}
              sx={{
                width: { xs: "300px", md: "700px" },
                height: { xs: "250px", md: "400px" },
              }}
            >
              <Typography fontSize={matches ? 34 : 24} sx={{ color: "#FFF" }}>
                Sueldo en USD :{!matches && <br></br>} ${" "}
                {valueSellDollar.toFixed(2)}
              </Typography>
              <Box
                display={"flex"}
                justifyContent={"center"}
                flexDirection={"row"}
                gap={10}
                sx={{ marginTop: { xs: "25px", md: "50px" } }}
              >
                <Link href={"/"} style={{ textDecoration: "none" }}>
                  <Button
                    variant="outlined"
                    sx={{ width: { xs: "100px", md: "250px" } }}
                  >
                    <Typography
                      fontStyle={"normal"}
                      fontSize={matches ? 19 : 12}
                      fontWeight={"bold"}
                      color={"#FFF"}
                    >
                      Volver al inicio
                    </Typography>
                  </Button>
                </Link>

                <Link href={"/calcular"} style={{ textDecoration: "none" }}>
                  <Button
                    variant="outlined"
                    sx={{ width: { xs: "100px", md: "250px" } }}
                  >
                    <Typography
                      fontStyle={"normal"}
                      fontSize={matches ? 19 : 12}
                      fontWeight={"bold"}
                      color={"#FFF"}
                    >
                      Volver a calcular
                    </Typography>
                  </Button>
                </Link>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Result;
