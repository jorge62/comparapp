import { Home } from "@/components/home";
import RootLayout from "./layout";
import Header from "@/components/ui/header";

const HomePage: React.FC = () => {
  return (
    <>
      <RootLayout>
        <main
          style={{
            height: "50vh",
            width: "100vw",
            overflow: "hidden",
            display: "flex",
            justifyContent: "center",
            marginTop: "50px",
          }}
        >
          <Header />
          <Home />
        </main>
      </RootLayout>
    </>
  );
};

export default HomePage;

export const metadata = {
  title: "ComparaApp",
};
