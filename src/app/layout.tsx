import "../../styles/globals.css";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html
      lang="en"
      style={{
        backgroundColor: "#004F92",
        maxWidth: "100%",
      }}
    >
      <body>{children}</body>
    </html>
  );
}
